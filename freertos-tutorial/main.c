
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"

#define DELAY_LOOP_COUNT 100000000

void vTask1( void *pvParameters )
{
  volatile uint32_t ul;
  for( ;; )
  {
    printf("Task 1 (continuous) is running\n");
    for( ul = 0; ul < DELAY_LOOP_COUNT; ul++ ) {
    }
  }
}

void vTask2( void *pvParameters )
{
  volatile uint32_t ul;
  for( ;; )
  {
    printf("Task 2 (continuous) is running\n");
    for( ul = 0; ul < DELAY_LOOP_COUNT; ul++ ) {
    }
  }
}

void vTask3( void *pvParameters )
{
  const TickType_t xDelay2000ms = pdMS_TO_TICKS( 2000 );
  TickType_t xLastWakeTime;
  xLastWakeTime = xTaskGetTickCount();
 
  for( ;; )
  {
    printf("Task 3 (periodic) is running\n");
    vTaskDelayUntil(&xLastWakeTime, xDelay2000ms);
  }
}

/*** SEE THE COMMENTS AT THE TOP OF THIS FILE ***/
int main( void )
{
    /* Perform any hardware setup necessary. (Not necessary for Desktop) */
    /* prvSetupHardware(); */
 
    /* --- APPLICATION TASKS CAN BE CREATED HERE --- */
    xTaskCreate( vTask1, "Task 1", 1000, NULL, 1, NULL);
    xTaskCreate( vTask2, "Task 2", 1000, NULL, 1, NULL);
    xTaskCreate( vTask3, "Task 3", 1000, NULL, 1, NULL);

 
    /* Start the created tasks running. */
    vTaskStartScheduler();
 
    /* Execution will only reach here if there was insufficient heap to
    start the scheduler. */
    for( ;; );
    return 0;
 }
/*-----------------------------------------------------------*/