#ifndef AQUECIMENTO_H_INCLUDED
#define AQUECIMENTO_H_INCLUDED

class Aquecimento {
  public:
  /************************
   liga
   Liga o sistema de aquecimento
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void liga(void) = 0;

  /************************
   desliga
   Desliga o sistema de aquecimento
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void desliga(void) = 0;

};

#endif // AQUECIMENTO_H_INCLUDED
