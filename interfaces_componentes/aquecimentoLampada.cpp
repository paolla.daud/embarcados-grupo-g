#include <Arduino.h>
#include "definicoes_sistema.h"
#include "aquecimentoLampada.h"

AquecimentoLampada::AquecimentoLampada() {}

  /************************
   liga
   Liga o LED vermelho que 
   simula o sistema de 
   aquecimento do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
void AquecimentoLampada::liga()
{
    digitalWrite(LAMPADA, ON);
    return;
}

  /************************
   desliga
   Desliga a o LED vermelho que 
   simula o sistema de 
   aquecimento do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
void AquecimentoLampada::desliga()
{
    digitalWrite(LAMPADA, OFF);
    return;
}