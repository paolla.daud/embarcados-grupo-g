#ifndef AQUECIMENTO_LAMPADA_H_INCLUDED
#define AQUECIMENTO_LAMPADA_H_INCLUDED

#include "aquecimento.h""

class AquecimentoLampada: public Aquecimento {
  public:
    AquecimentoLampada();
  	void liga();
  	void desliga();
};


#endif // AQUECIMENTO_LAMPADA_H_INCLUDED