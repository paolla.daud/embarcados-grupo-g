#ifndef BLUETOOTH_H_INCLUDED
#define BLUETOOTH_H_INCLUDED

class Bluetooth {
  public:

  /************************
   setup
   Inicializa o canal de 
   comunicação com a aplicação
   plantário
  entradas - nenhuma
  saidas -  nenhuma
  *************************/
  virtual void setup(void) = 0;

  /************************
   recebe
   Recebe comando do usuário
   enviado pela aplicação
  entradas -  nenhuma
  saidas - char lido
  *************************/
  virtual char recebe(void) = 0;

  /************************
   envia
   Envia mensagens para o 
   o usuário na aplicação
  entradas -  char* mensagem
  saidas - nenhuma
  *************************/
  virtual void envia(char* mensagem) = 0;

};


#endif // BLUETOOTH_H_INCLUDED
