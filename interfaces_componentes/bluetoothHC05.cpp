#include <Arduino.h>

#include "definicoes_sistema.h"
#include "bluetoothHC05.h"


BluetoothHC05::BluetoothHC05(){}

  /************************
   setup
   Inicializa o canal de 
   comunicação com a aplicação
   plantário
   entradas - nenhuma
   saidas -  nenhuma
  *************************/
void BluetoothHC05::setup(void)
{
  IHM_BT.begin(BAUDRATE);
  IHM_BT.print("Comunicação bluetooth estabelecida com sucesso \0");
  IHM_BT.print(" BEM-VINDO AO SEU PLANTARIO \0");
}

  /************************
   recebe
   Recebe comando do usuário
   enviado pela aplicação
  entradas -  nenhuma
  saidas - char lido
  *************************/
char BluetoothHC05::recebe()
{
  if (IHM_BT.available()) {
    char c = IHM_BT.read();
    return(c);
    }
}

  /************************
   envia
   Envia mensagens para o 
   o usuário na aplicação
  entradas -  char* mensagem
  saidas - nenhuma
  *************************/
void BluetoothHC05::envia(char* mensagem)
{
  if (IHM_BT.available()) {
    IHM_BT.print(mensagem);
    return;
    }
}