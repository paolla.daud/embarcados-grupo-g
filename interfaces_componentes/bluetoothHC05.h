#ifndef BLUETOOTH_HC05_H_INCLUDED
#define BLUETOOTH_HC05_H_INCLUDED

#include "bluetooth.h""

class BluetoothHC05: public Bluetooth {
  public:
  BluetoothHC05();
  void setup();
  void envia(char* mensagem);
  char recebe();

};

#endif // BLUETOOTH_HC05_H_INCLUDED
