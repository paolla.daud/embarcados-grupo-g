#ifndef ILUMINACAO_H_INCLUDED
#define ILUMINACAO_H_INCLUDED

class Iluminacao {
  public:
  /************************
   liga
   Acende o sistema de iluminação
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void liga(void) = 0;

  /************************
   desliga
   Apaga o sistema de iluminação
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void desliga(void) = 0;

};

#endif // ILUMINACAO_H_INCLUDED
