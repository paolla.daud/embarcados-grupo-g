#include <Arduino.h>
#include "definicoes_sistema.h"
#include "iluminacaoLED.h"

IluminacaoLED::IluminacaoLED() {}

  /************************
   liga
  Acende os LEDs brancos de
  iluminação do plantário
  entradas - nenhuma
  saidas - nenhuma
  *************************/
void IluminacaoLED::liga()
{
    for(int led_i = LED_MIN; led_i<=LED_MAX; led_i++){
        digitalWrite(led_i, ON);
    }
    return;
}

  /************************
   desliga
  Desliga os LEDs brancos de
  iluminação do plantário
  entradas - nenhuma
  saidas - nenhuma
  *************************/
void IluminacaoLED::desliga()
{
    for(int led_i = LED_MIN; led_i<=LED_MAX; led_i++){
        digitalWrite(led_i, OFF);
    }
    return;
}