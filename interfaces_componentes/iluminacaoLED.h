#ifndef ILUMINACAO_LED_H_INCLUDED
#define ILUMINACAO_LED_H_INCLUDED

#include "iluminacao.h""

class IluminacaoLED: public Iluminacao {
  public:
    IluminacaoLED();
  	void liga();
  	void desliga();
};


#endif // ILUMINACAO_LED_H_INCLUDED
