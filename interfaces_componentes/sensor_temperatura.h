#ifndef SENSOR_TEMPERATURA_H_INCLUDED
#define SENSOR_TEMPERATURA_H_INCLUDED

class SensorTemperatura {
  public:
  /************************
   leitura
   Le o valor do sensor
   Entradas -  nenhuma
   Saidas - float : leitura da 
   temperatura (já convertida para C) 
  *************************/
  virtual float leitura(void) = 0;
  
};

#endif // SENSOR_TEMPERATURA_H_INCLUDED