#include <Arduino.h>
#include "definicoes_sistema.h"
#include "sensor_temperatura_LM35.h""

SensorTemperaturaLM35::SensorTemperaturaLM35()
{
}

/************************
 SensorTemperaturaTMP36::leitura
Le o valor do sensor
Entradas - nenhuma
Saidas - float : leitura da 
temperatura (já convertida para C) 
*************************/
float SensorTemperaturaTMP36::leitura()
{
    return((float)analogRead(SENSOR) * 0.48828125);
}


