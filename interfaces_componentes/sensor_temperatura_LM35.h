#ifndef SENSOR_TEMPERATURA_LM35_H_INCLUDED
#define SENSOR_TEMPERATURA_LM35_H_INCLUDED

#include "sensor_temperatura.h"

class SensorTemperaturaLM35: public SensorTemperatura{
  public:
    SensorTemperaturaLM35();
    float leitura();
};


#endif // SENSOR_TEMPERATURA_LM35_H_INCLUDED