/*
    PLANTÁRIO
*/

/**********************************************************************************
                                    DEFINIÇOES
**********************************************************************************/
//Para outro microcontrolador que não o arduino, seria definido no: #include "definicoes_sistema.h"
//#include "definicoes_sistema.h"

#define true  1
#define false 0
#define ON 1
#define OFF 0


#define NUM_ESTADOS 2
#define NUM_EVENTOS 4

// ESTADOS
#define ESPERA       0
#define REGULAGEM    1

// EVENTOS
#define NENHUM_EVENTO -1
#define PRIMAVERA      0
#define VERAO          1
#define OUTONO         2
#define INVERNO        3

// ACOES
#define NENHUMA_ACAO -1
#define A01  0 //EXPOSIÇÃO DE LUZ: 12H TEMPERATURA: 18-22.9C
#define A02  1 //EXPOSIÇÃO DE LUZ: 13H30M TEMPERATURA 23-28C
#define A03  2 //EXPOSIÇÃO DE LUZ: TEMPERATURA 12H 16-19.9C
#define A04  3 //EXPOSIÇÃO DE LUZ: 10H30M TEMPERATURA 10-15.9C

// VALORES DE REFERENCIA PARA TEMPERATURA E EXPOSIÇÃO A LUZ
#define TMP_MAX_PRI 23 //temperatura maxima primavera
#define TMP_MIN_PRI 18 //temperatura minima primavera
#define SOL_PRI 2880000  //períodos de 15ms em 12H
#define TMP_MAX_VERAO 28 //temperatura maxima verão
#define TMP_MIN_VERAO 23 //temperatura minima verão
#define SOL_VERAO  3240000; //períodos de 15ms em 13H30M
#define TMP_MAX_OUT 20 //temperatura maxima outono
#define TMP_MIN_OUT 16 //temperatura minima outono
#define SOL_OUT 2880000  //períodos de 15ms em 12H
#define TMP_MAX_INV 15 //temperatura maxima inverno
#define TMP_MIN_INV 10 //temperatura minima inverno
#define SOL_INV 2760000 //períodos de 15ms em 11H30M
#define DIA 1 //estado de dia representado como 1
#define NOITE 0 //estado de noite representado como 0

// DEFINIÇÕES - PINOS CORRESPONDENTES A CADA COMPONENTE
#define SENSOR A0
#define IHM_BT Serial1
#define LED_MIN 3
#define LED_MAX 5
#define LAMPADA 7
#define PINO_VENTOINHA 31
#define BAUDRATE 9600


/**********************************************************************************
                            INCLUDE DE BIBLIOTECAS .c e FreeRTOS
**********************************************************************************/

#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include <EEPROM.h> //para poder utilziar a memoria eeprom do arduino
#include <Servo.h> //Include da Biblioteca para controle da Ventoinha (Componente ServoMotor)

/**********************************************************************************
                VARIAVEIS GLOBAIS (menos de máquina de estados) E EEPROM
**********************************************************************************/

int tmp_sol = 0; //registra clocks de quando a luz está ligada
int tmp_esc = 0; //registra clocks de quando a luz está desligada
int periodo = DIA; //Define o período
int cont_5m_sol; //Contagem de ciclos de 5m em claro para registro na eeprom
int cont_5m_esc; //Contagem de ciclos de 5m em escuro para registro na eeprom

/***********************************************************************
                                    ESTÁTICOS
Inicialização das variáveis da máquina de estados
 ***********************************************************************/

int codigoEvento = NENHUM_EVENTO;
int eventoInterno = NENHUM_EVENTO;
int estado = ESPERA;
int codigoAcao;
int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];

int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS] = 
{
    {REGULAGEM, REGULAGEM, REGULAGEM, REGULAGEM},
    {REGULAGEM, REGULAGEM, REGULAGEM, REGULAGEM}
};

int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS] = 
{
    {A01, A02, A03, A04},
    {A01, A02, A03, A04}
};

/**********************************************************************************
                INCLUDE DE ARQUIVOS DE COMPONENTES E DEFINIÇÃO
**********************************************************************************/
//Essas funções seriam adicionadas em caso de outros micro-controladores que não o Arduino

/* ARQUIVOS RELATIVOS A CLASSE SENSOR TEMPERATURA LM35*/
/*-------------------------------------------------------------------------------*/
/* "arquivo" sensor_temperatura.h 
* #ifndef SENSOR_TEMPERATURA_H_INCLUDED
* #define SENSOR_TEMPERATURA_H_INCLUDED  */

class SensorTemperatura {
  public:
  /************************
   leitura
   Le o valor do sensor
   Entradas -  nenhuma
   Saidas - float : leitura da 
   temperatura (já convertida para C) 
  *************************/
  virtual float leitura(void) = 0;
};

// #endif // SENSOR_TEMPERATURA_H_INCLUDED

/*-------------------------------------------------------------------------------*/

/* "arquivo" sensor_temperatura_LM35.h
* #ifndef SENSOR_TEMPERATURA_LM35_H_INCLUDED
* #define SENSOR_TEMPERATURA_LM35_H_INCLUDED
*/

// #include "sensor_temperatura.h"

class SensorTemperaturaLM35: public SensorTemperatura{
  public:
    SensorTemperaturaLM35();
    float leitura();
};

//#endif // SENSOR_TEMPERATURA_LM35_H_INCLUDED

/*-------------------------------------------------------------------------------*/

/* "arquivo" sensor_temperatura_LM35.cpp
* #include <Arduino.h>
* #include "definicoes_sistema.h"
* #include "sensor_temperatura_LM35.h"
*/

SensorTemperaturaLM35::SensorTemperaturaLM35(){}

/************************
 SensorTemperaturaTMP36::leitura
Le o valor do sensor
Entradas - nenhuma
Saidas - float : leitura da 
temperatura (já convertida para C) 
*************************/
float SensorTemperaturaTMP36::leitura()
{
    return((float)analogRead(SENSOR) * 0.48828125);
}

/*-------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------*/

/* ARQUIVOS RELATIVOS A CLASSE AQUECIMENTO LAMPADA*/

/* "arquivo" aquecimento.h
#ifndef AQUECIMENTO_H_INCLUDED
#define AQUECIMENTO_H_INCLUDED */

class Aquecimento {
  public:
  /************************
   liga
   Liga o sistema de aquecimento
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void liga(void) = 0;

  /************************
   desliga
   Desliga o sistema de aquecimento
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void desliga(void) = 0;

};

//#endif // AQUECIMENTO_H_INCLUDED

/*-------------------------------------------------------------------------------*/
/* "arquivo" aquecimentoLampada.h
#ifndef AQUECIMENTO_LAMPADA_H_INCLUDED
#define AQUECIMENTO_LAMPADA_H_INCLUDED
*/

//#include "aquecimento.h""

class AquecimentoLampada: public Aquecimento {
  public:
    AquecimentoLampada();
  	void liga();
  	void desliga();
};
//#endif // AQUECIMENTO_LAMPADA_H_INCLUDED

/*-------------------------------------------------------------------------------*/
/* "arquivo" aquecimentoLampada.cpp
* #include <Arduino.h>
* #include "definicoes_sistema.h"
* #include "aquecimentoLampada.h" */

AquecimentoLampada::AquecimentoLampada() {}

  /************************
   liga
   Liga o LED vermelho que 
   simula o sistema de 
   aquecimento do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
void AquecimentoLampada::liga()
{
    digitalWrite(LAMPADA, ON);
    return;
}

  /************************
   desliga
   Desliga a o LED vermelho que 
   simula o sistema de 
   aquecimento do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
void AquecimentoLampada::desliga()
{
    digitalWrite(LAMPADA, OFF);
    return;
}

/*-------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------*/

/* ARQUIVOS RELATIVOS A CLASSE ILUMINACAO LED */
/*-------------------------------------------------------------------------------*/
/* "arquivo" iluminacao.h 
* #ifndef ILUMINACAO_H_INCLUDED
* #define ILUMINACAO_H_INCLUDED  */

class Iluminacao {
  public:
  /************************
   liga
   Acende o sistema de iluminação
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void liga(void) = 0;

  /************************
   desliga
   Apaga o sistema de iluminação
   do plantário
   entradas - nenhuma
   saidas - nenhuma
  *************************/
  virtual void desliga(void) = 0;

};
//#endif // ILUMINACAO_H_INCLUDED

/*-------------------------------------------------------------------------------*/

/* "arquivo" iluminacaoLED.h
* #ifndef ILUMINACAO_LED_H_INCLUDED
* #define ILUMINACAO_LED_H_INCLUDED */

//#include "iluminacao.h""
class IluminacaoLED: public Iluminacao {
  public:
    IluminacaoLED();
  	void liga();
  	void desliga();
};
//#endif // ILUMINACAO_LED_H_INCLUDED

/*-------------------------------------------------------------------------------*/

/* "arquivo" iluminacaoLED.cpp 
* #include <Arduino.h>
* #include "definicoes_sistema.h"
* #include "iluminacaoLED.h" */

IluminacaoLED::IluminacaoLED() {}
  /************************
   desliga
  Acende os LEDs brancos de
  iluminação do plantário
  entradas - nenhuma
  saidas - nenhuma
  *************************/
void IluminacaoLED::liga()
{
    for(int led_i = LED_MIN; led_i<=LED_MAX; led_i++){
        digitalWrite(led_i, ON);
    }
    return;
}
  /************************
   desliga
  Desliga os LEDs brancos de
  iluminação do plantário
  entradas - nenhuma
  saidas - nenhuma
  *************************/
void IluminacaoLED::desliga()
{
    for(int led_i = LED_MIN; led_i<=LED_MAX; led_i++){
        digitalWrite(led_i, OFF);
    }
    return;
}

/*-------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------*/

/* ARQUIVOS RELATIVOS A CLASSE IHM BLUETOOTH*/

/* "arquivo" bluetooth.h
#ifndef BLUETOOTH_H_INCLUDED
#define BLUETOOTH_H_INCLUDED */

class Bluetooth {
  public:

  /************************
   setup
   Inicializa o canal de 
   comunicação com a aplicação
   plantário
  entradas - nenhuma
  saidas -  nenhuma
  *************************/
  virtual void setup(void) = 0;

  /************************
   recebe
   Recebe comando do usuário
   enviado pela aplicação
  entradas -  nenhuma
  saidas - char lido
  *************************/
  virtual char recebe(void) = 0;

  /************************
   envia
   Envia mensagens para o 
   o usuário na aplicação
  entradas -  char* mensagem
  saidas - nenhuma
  *************************/
  virtual void envia(char* mensagem) = 0;

};
//#endif // BLUETOOTH_H_INCLUDED

/*-------------------------------------------------------------------------------*/
/* "arquivo" bluetoothHC05.h
#ifndef BLUETOOTH_HC05_H_INCLUDED
#define BLUETOOTH_HC05_H_INCLUDED */

//#include "bluetooth.h""

class BluetoothHC05: public Bluetooth {
  public:
  BluetoothHC05();
  void setup();
  void envia(char* mensagem);
  char recebe();

};

//#endif // BLUETOOTH_HC05_H_INCLUDED

/*-------------------------------------------------------------------------------*/
/* "arquivo" BluetoothHC05.cpp
#include <Arduino.h>
#include "definicoes_sistema.h"
#include "bluetoothHC05.h" */

BluetoothHC05::BluetoothHC05(){}

  /************************
   setup
   Inicializa o canal de 
   comunicação com a aplicação
   plantário
   entradas - nenhuma
   saidas -  nenhuma
  *************************/
void BluetoothHC05::setup(void)
{
  IHM_BT.begin(BAUDRATE);
  IHM_BT.print("Comunicação bluetooth estabelecida com sucesso \0");
  IHM_BT.print(" BEM-VINDO AO SEU PLANTARIO \0");
}
  /************************
   recebe
   Recebe comando do usuário
   enviado pela aplicação
  entradas -  nenhuma
  saidas - char lido
  *************************/
char BluetoothHC05::recebe()
{
  if (IHM_BT.available()) {
    char c = IHM_BT.read();
    return(c);
    }
}
  /************************
   envia
   Envia mensagens para o 
   o usuário na aplicação
  entradas -  char* mensagem
  saidas - nenhuma
  *************************/
void BluetoothHC05::envia(char* mensagem)
{
  if (IHM_BT.available()) {
    IHM_BT.print(mensagem);
    return;
    }
}
/*-------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------*/

/* CLASSE VENTOINHA SERVO => como estamos usando a classe Servo do arduino, não é 
* necessário criar uma nova classe e subclasse para esse componente  */


/**********************************************************************************
                            DECLARAÇÃO DE COMPONENTES
**********************************************************************************/
//Em um microprocessador que não o Arduino, aqui seriam definidas as classes de cada componente,
// a partir dos includes do item anterior. No caso do arduino, aqui são definidas as funções de cada um

SensorTemperaturaLM35 sensor;
AquecimentoLampada lampada;
IluminacaoLED leds;
BluetoothHC05 bluetooth;
Servo ventoinha;

//Funcao Motor CC:
void iniciaVentoinha(void){
    ventoinha.attach(PINO_VENTOINHA);
}
void ligaVentoinha(void){
    ventoinha.write(170);
}
void desligaVentoinha(void){
    ventoinha.write(89); //checar se fica parado com esse valor
}


/************************************************************************
 *                      DECLARAÇÃO FUNÇÕES
*************************************************************************/
//Seria usado no caso de microprocessadores com mais arquivos
//int obterEvento(void);
//int obterAcao(int estado, int codigoEvento);
//int obterProximoEstado(int estado, int codigoEvento);
//void executarAcao(int codigoAcao);

//Escrita EEPROM
int epr_write(int cont_5m_sol, int cont_5m_esc){
    EEPROM.write(0, cont_5m_sol);
    EEPROM.write(1, cont_5m_esc);
}

//Leitura EEPROM
int epr_read(){
    cont_5m_sol = EEPROM.read(0);
    cont_5m_esc = EEPROM.read(1);
}


/***********************************************************************
 FreeRTOS
 ***********************************************************************/
void taskMaqEstados(void *pvParameters);
void taskObterEvento(void *pvParameters);
QueueHandle_t xQueue;
SemaphoreHandle_t xBinarySemaphore;
TaskHandle_t xTaskMaqEstados, xTaskObterEvento;


/************************************************************************
 executarAcao
 Executa uma acao
 Parametros de entrada:
    (int) codigo da acao a ser executada
 Retorno: (int) codigo do evento interno ou NENHUM_EVENTO
*************************************************************************/
int executarAcao(int codigoAcao)
{
    int retval;
    int tmp_max;
    int tmp_min;
    int periodo_sol;

    retval = NENHUM_EVENTO;

    if (codigoAcao == NENHUMA_ACAO)
        return retval;

    switch(codigoAcao) {

    case A01: //Caso da Primavera
        tmp_max = TMP_MAX_PRI;
        tmp_min = TMP_MIN_PRI;
        periodo_sol = SOL_PRI;
        break;

    case A02: //Caso Verao
        tmp_max = TMP_MAX_VERAO;
        tmp_min = TMP_MIN_VERAO;
        periodo_sol = SOL_VERAO;
        break;

    case A03: //Caso Outono
        tmp_max = TMP_MAX_OUT;
        tmp_min = TMP_MIN_OUT;
        periodo_sol = SOL_OUT;
        break;

    case A04: //Caso Inverno
        tmp_max = TMP_MAX_INV;
        tmp_min = TMP_MIN_INV;
        periodo_sol = SOL_INV;
        break;
    }

    if (tmp_sol > 19999) { 
        if ((tmp_sol/20000) % 5 = 0){
            epr_write(tmp_sol, tmp_esc);
        }
    }

    if (periodo = DIA){
        tmp_sol += 1; //Mais um ciclo de clock iluminado
        tmp_esc -= 1; //Menos um ciclo de clock desiluminado
        if (tmp_sol > periodo_sol){ 
            //DESLIGAR LUZ
            leds.desliga();
            periodo = NOITE;
        }
    }

    if (periodo = NOITE){
        tmp_sol -= 1; //Menos um ciclo de clock iluminado
        tmp_esc += 1; //Mais um ciclo de clock iluminado
        if (tmp_esc > 0){
            //LIGAR LUZ
            leds.liga();
            periodo = DIA;
        }
    }

    if (sensor.leitura()> tmp_max){
        //DESLIGA LAMPADA CALOR
        lampada.desliga();
        //LIGA VENTOINHA
        ligaVentoinha();
    }

    if (sensor.leitura() < tmp_min){
        //LIGA LAMPADA CALOR
        lampada.liga();
        //DESLIGA VENTOINHA
        desligaVentoinha();
    }

    return retval;
} // executarAcao


/************************************************************************
 iniciaMaquina de Estados
 Carrega a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void iniciaMaquinaEstados()
{

  int i;
  int j;

  for (i=0; i < NUM_ESTADOS; i++) {
    for (j=0; j < NUM_EVENTOS; j++) {
       acao_matrizTransicaoEstados[i][j] = NENHUMA_ACAO;
       proximo_estado_matrizTransicaoEstados[i][j] = i;
    }
  }
  proximo_estado_matrizTransicaoEstados[ESPERA][PRIMAVERA] = REGULAGEM;
  acao_matrizTransicaoEstados[ESPERA][PRIMAVERA] = A01;

  proximo_estado_matrizTransicaoEstados[ESPERA][VERAO] = REGULAGEM;
  acao_matrizTransicaoEstados[ESPERA][VERAO] = A02;

  proximo_estado_matrizTransicaoEstados[ESPERA][OUTONO] = REGULAGEM;
  acao_matrizTransicaoEstados[ESPERA][OUTONO] = A03;

  proximo_estado_matrizTransicaoEstados[ESPERA][INVERNO] = REGULAGEM;
  acao_matrizTransicaoEstados[ESPERA][INVERNO] = A04;

  proximo_estado_matrizTransicaoEstados[REGULAGEM][PRIMAVERA] = REGULAGEM;
  acao_matrizTransicaoEstados[REGULAGEM][PRIMAVERA] = A01;

  proximo_estado_matrizTransicaoEstados[REGULAGEM][VERAO] = REGULAGEM;
  acao_matrizTransicaoEstados[REGULAGEM][VERAO] = A02;

  proximo_estado_matrizTransicaoEstados[REGULAGEM][OUTONO] = REGULAGEM;
  acao_matrizTransicaoEstados[REGULAGEM][OUTONO] = A03;

  proximo_estado_matrizTransicaoEstados[REGULAGEM][INVERNO] = REGULAGEM;
  acao_matrizTransicaoEstados[REGULAGEM][INVERNO] = A04;

} // initStateMachine


/************************************************************************
 iniciaSistema
 Inicia o sistema ...
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void iniciaSistema()
{
    iniciaMaquinaEstados();
    cont_5m_sol, cont_5m_esc = epr_read(); //Resgata tempo em claro e escuro da inicialização anterior
    bluetooth.setup(); //Setup do Bluetooth
    leds.liga(); //Normal iluminção ligada.

} // initSystem

/************************************************************************
 obterAcao
 Obtem uma acao da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo da acao
*************************************************************************/
int obterAcao(int estado, int codigoEvento) {
  return acao_matrizTransicaoEstados[estado][codigoEvento];
}

/************************************************************************
 obterProximoEstado
 Obtem o proximo estado da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo do estado
*************************************************************************/
int obterProximoEstado(int estado, int codigoEvento) {
  return proximo_estado_matrizTransicaoEstados[estado][codigoEvento];
}

/***********************************************************************
 Tasks
 ***********************************************************************/

/************************************************************************
 taskMaqEstados
 Task principal de controle que executa a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void taskMaqEstados(void *pvParameters){
    int codiggoEvento;
    BaseType_t xStatus;

    for( ;; ) {
        if( xQueueReceive( xQueue, &codigoEvento, portMAX_DELAY ) == pdPASS ) {
            if (eventoInterno == NENHUM_EVENTO) {
                bluetooth.envia("(0)PRIMAVERA (1)VERAO (2)OUTONO (3)INVERNO \0");
                codigoEvento = bluetooth.recebe();
                }
            else {
                codigoEvento = eventoInterno;
                }
            if (codigoEvento != NENHUM_EVENTO){
                codigoAcao = obterAcao(estado, codigoEvento);
                estado = obterProximoEstado(estado, codigoEvento);
                eventoInterno = executarAcao(codigoAcao);
                Serial.print("Estado: ");
                Serial.print(estado);
                Serial.print(" Evento: ");
                Serial.print(codigoEvento);
                Serial.print(" Acao: ");
                Serial.println(codigoAcao);
  }
}
/************************************************************************
 taskObterEvento
 Task que faz pooling de eventos
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void taskObterEvento(void *pvParameters){
    
    int codigoEvento;
    BaseType_t xStatus;

    for( ;; ) {        
        codigoEvento = NENHUM_EVENTO;
      
        char opcao = bluetooth.recebe();
        if (opcao == '0') {
            codigoEvento = PRIMAVERA;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            continue;
        }
        if (opcao == '1') {
            codigoEvento = VERAO;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            continue;
        }
        if (opcao == '2') {
            codigoEvento = OUTONO;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            continue;
        }
        if (opcao == '3') {
            codigoEvento = INVERNO;
            xStatus = xQueueSendToBack( xQueue, &codigoEvento, 0 );
            continue;
        }
    }
}

/************************************************************************
*                      FUNCOES ARDUINO ("MAIN" : SETUP E LOOP)
* Setup e Loop principal de controle que executa a maquina de estados
* Parametros de entrada: nenhum
* Retorno: nenhum
*
*************************************************************************/

void setup() {

  Serial.begin(9600);

  iniciaSistema(); //Inicia o Sistema

  // configure tasks
  xTaskCreate(taskMaqEstados,"taskMaqEstados", 150, NULL, 2, &xTaskMaqEstados);
  xTaskCreate(taskObterEvento,"taskObterEvento", 100, NULL, 1, &xTaskObterEvento);
  vTaskStartScheduler();

}


void loop() {
}

